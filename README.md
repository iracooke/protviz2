ProtViz2
========

A visualisation plugin for Tandem Mass Spectrometry data, specifically, Peptides and Proteins identified along with their corresponding MS/MS spectra.

The starting point for visualizing data is to convert it to an SQLite database.  No formal schema for this database exists yet, but there is a ruby script for creating a database from a ProtXML file and associated spectrum files.  This script is part of [protk](https://github.com/iracooke/protk).

# Status

Proof of concept demo only

# Generating an example sqlite file from the test data

Currently the testdata is not part of the source code repository so it needs to be obtained separately. 
TODO: Provide valid test data that can be shared publicly

First install protk.  You will need an unreleased build of version 1.3.1

```bash
	gem install protk -v 1.3.1.pre3
```

In order to run the protxml_to_psql script you need blast installed on your local system.  If it isn't already installed you can install using protk as follows

```bash
	protk_setup.rb blast
```

Now process the test data

```bash	
	cd testdata
	protxml_to_psql.rb -d DBAlt.fasta S_01_tandem_pproph_protproph.prot.xml -o S01.sqlite S_01.mzML
```

# Installing the plugin to galaxy

Within your galaxy installation create a folder for the protviz2 plugin

```bash
	mkdir config/plugins/visualizations/protviz2
```

Copy the contents of the `plugin` directory to this folder


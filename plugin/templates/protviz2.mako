<%
	path=hda.file_name
	protein_groups=hda.datatype.dataprovider(hda,'sqlite',query="select * from Proteins LEFT JOIN ProteinGroups ON Proteins.ProteinGroupID=ProteinGroups.ID limit 100")
%>
## ----------------------------------------------------------------------------
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Title</title>


</head>

<body>

	<!-- Extremely bare-bones for demo purposes. 
		Each group object is of type sqlite3.Row and represents a row returned by the sqlite query.
		A Row object behaves much like a dictionary. You can get a list of its keys with .keys().
	-->

	<ul>
	% for group in protein_groups:
		% if group is not None:
			<li>
				${group['Name'] | h}, 
				${group['Description'] | h}
				${group['Probability'] | h}
			</li>
		% endif
	% endfor
	</ul>

</body>
